<?php

namespace App\Support;

use App\Models\User;

class Auth {

    public static function attempt($email, $password) {

        $user = User::where('email', $email);

        if (!$user->exists()) {
            return false;
        };

        $user = $user->first();

        if ($password != $user->password) {
            dd('wrong password');

            return false;
        }

        $_SESSION['user'] = [
            'id'       => $user->id,
            'name'     => $user->name,
            'password' => $user->password,
        ];

        return true;
    }

    public static function logout() {

        $_SESSION['user'] = [
            'id'       => null,
            'name'     => null,
            'password' => null,
        ];

        return self::guest();
    }

    public static function user() {

        if(!isset($_SESSION['user'])){
            return false;
        }
        $query = User::where($_SESSION['user']);

        return $query->exists() ? $query->first() : false;
    }

    public static function check() {

        return (bool) self::user();

    }

    public static function guest() {

        return (bool) !self::check();

    }


}
