<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserService extends Model {

    public $timestamps = false;

    protected $fillable = ['user_id', 'service_id'];


    protected $table = 'user_service';


}