<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Service extends Model {


    protected $fillable = ['title', 'description', 'category_id', 'user_id', 'price'];

    public function image_path() {

//        return '/images/'.$this->id.'.jpg';
        return '/images/6.jpg';

    }

    public function user() {

        return $this->belongsTo(User::class, 'user_id', 'id');

    }

    public function users() {
        return $this->belongsToMany(Service::class, 'user_service');
    }

    public function canEdit() {

        return $this->canRemove();

    }

    public function canRemove() {

        return $this->isOwner() or Auth::user()->isAdmin();
    }

    public function isOwner() {

        return $this->user_id === Auth::user()->id;
    }
}