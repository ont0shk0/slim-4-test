<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

    const ROLE_USER = 0;
    const ROLE_MODERATOR = 1;
    const ROLE_ADMIN = 2;

    protected $fillable = ['name', 'email', 'role'];

    protected $guarded = ['password'];

    public function services() {

        return $this->hasMany(Service::class, 'user_id', 'id');

    }

    public function subscribes() {

         return $this->belongsToMany(Service::class, 'user_service', 'user_id', 'service_id');
    }

    public function isUser(){
        return $this->role === self::ROLE_USER;
    }

    public function isModerator() {

        return $this->role == self::ROLE_MODERATOR;
    }

    public function isAdmin() {

        return $this->role == self::ROLE_ADMIN;
    }
}