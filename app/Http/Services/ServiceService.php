<?php


namespace App\Http\Services;


use App\Models\Service;

class ServiceService {

    public function add($title, $description, int $user_id, int $category_id, int $price) {

        Service::create([
            'title'       => $title,
            'description' => $description,
            'user_id'     => $user_id,
            'category_id' => $category_id,
            'price'       => $price,
        ]);

        return true;

    }

    public function update(Service $service, $title, $description, int $user_id, int $category_id, int $price) {
         return $service->update([
            'title'       => $title,
            'description' => $description,
            'user_id'     => $user_id,
            'category_id' => $category_id,
            'price'       => $price,
        ]);

    }

    public function remove(Service $service) {

        return $service->delete();
    }
}