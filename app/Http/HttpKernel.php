<?php


namespace App\Http;


use App\Http\Middleware\RedirectIfGuestMiddleware;
use Boot\Foundation\HttpKernel as Kernel;

class HttpKernel extends Kernel {

    /*
     * global middleware
     *
     * @var array
     */
    public array $middleware = [
//        Middleware\ExampleAfterMiddleware::class,
//        Middleware\ExampleBeforeMiddleware::class
    ];

    public array $middlewareGroups = [
        'api' => [],
        'web' => [
            Middleware\RouteContextMiddleware::class,
        ]
    ];

}