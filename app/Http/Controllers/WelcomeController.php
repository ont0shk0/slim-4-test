<?php


namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserService;
use App\Support\View;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use DB;

class WelcomeController {

    public function index(View $view) {

        return $view( 'index');

    }

    public function home(View $view, Request $request, Response $response, $name){

        return $view( 'auth.home', compact('name'));
    }

    public function test($response){
        $us = new UserService();
        $us->user_id = 1;
        $us->service_id = 1;
        $us->save();
//        UserService::create([
//            'service_id' => 1, //$service_id,
//            'user_id'    => 2, // $user_id,
//        ]);
        return $response;
    }
}