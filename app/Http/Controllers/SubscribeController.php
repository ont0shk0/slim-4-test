<?php

namespace App\Http\Controllers;

use Auth;

class SubscribeController {

    public function add($id) {

        Auth::user()->subscribes()->syncWithoutDetaching($id);

        return redirect('/service/' . $id);
    }

    public function remove($id) {

        Auth::user()->subscribes()->detach($id);

        return redirect('/service/' . $id);

    }

}