<?php


namespace App\Http\Controllers;

use App\Support\RequestInput;
use App\Support\View;
use Auth;

class LoginController {

    public function show(View $view) {

        return $view('auth.login');

    }

    public function store(RequestInput $input) {

        $success = Auth::attempt($input->email, sha1($input->password));

        if (!$success) {
            dd('Login fail...');
        }

        return redirect('/dashboard');

    }

    public function logout() {

        Auth::logout();

        return redirect('/login');

    }

}