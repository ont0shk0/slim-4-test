<?php


namespace App\Http\Controllers;

use App\Models\Service;
use App\Support\View;
use Auth;

class DashboardController {

    public function index(View $view) {

        $services = Service::all();
        return $view( 'dashboard.index', compact('services'));

    }

    public function own(View $view) {

        $services = Auth::user()->services()->get();
        return $view( 'dashboard.own', compact('services'));

    }

    public function subscribes(View $view) {

        $services = Auth::user()->subscribes()->get();
        return $view( 'dashboard.subscribe', compact('services'));

    }

}