<?php


namespace App\Http\Controllers;

use App\Models\User;
use App\Support\RequestInput;
use App\Support\View;
use Auth;

class RegisterController {

    public function show(View $view) {

        return $view( 'auth.register');

    }

    public function store(RequestInput $input, User $user){
        if($input->password != $input->passwordConfirm){
            dd('password wrong!!');
        }
        $input->forget('passwordConfirm');

        $input->password = sha1($input->password);

        if($user::where('email', $input->email)->exists()){
            dd("User with {$input->email} already exists");
        }

        $user = $user::forceCreate($input->all());

        Auth::attempt($user->email, $input->password);

        return redirect('/dashboard');
    }

}