<?php

namespace App\Http\Controllers;
use App\Http\Services\ServiceService;
use App\Models\Service;
use App\Support\RequestInput;
use App\Support\View;
use Auth;

class ServiceController {

    /**
     * @var ServiceService
     */
    private ServiceService $service;

    public function __construct(ServiceService $service) {
        $this->service = $service;
    }

    public function create(View $view) {

        return $view('service.create');
    }

    public function store(RequestInput $input) {

        $this->service->add($input->title, $input->description, Auth::user()->id, $input->category, $input->price);

        return redirect('/dashboard/own');
    }

    public function show(View $view, $id) {
        $service = Service::where('id', $id)->first();
        if(!$service){
            return redirect('/dashboard/own');
        }
        $contains = Auth::user()->subscribes->contains($id);

        return $view('service.show', compact('service', 'contains'));

    }

    public function edit(View $view, $id) {
        $service = Service::where('id', $id)->first();
        if(!$service){
            return redirect('/dashboard/own');
        }
        return $view('service.edit', compact('service'));

    }


    public function update($id, RequestInput $input) {
        $service = Service::where('id', $id )->first();

        $this->service->update($service, $input->title, $input->description, Auth::user()->id, $input->category, $input->price);

        return redirect('/dashboard/own');
    }


    public function remove(RequestInput $input) {

        $service = Service::where(['id' => $input->id, 'user_id' => Auth::user()->id])->first();
        if($service AND $service->canRemove()){
            $this->service->remove($service);
        }

        return redirect('/dashboard/own');

    }





}