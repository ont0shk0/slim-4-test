<?php


namespace App\Http\Controllers;


use App\Models\User;
use Psr\Http\Message\ResponseInterface as Response;

class ApiController {

    public function example(Response $response, User $user) {

        $response->getBody()->write(json_encode($user->get(), JSON_PRETTY_PRINT));

        return $response;
    }



}