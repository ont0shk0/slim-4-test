<?php


namespace App\Http\Middleware;

use Slim\Psr7\Response;


use Psr\Http\Server\RequestHandlerInterface as Handle;
use Psr\Http\Message\ServerRequestInterface as Request;

class ExampleAfterMiddleware {

    public function __invoke(Request $request, Handle $handle) : Response {

        $response = $handle->handle($request);

        $response->getBody()->write(" \n after middleware");

        return $response;
    }
}