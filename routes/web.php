<?php
use App\Support\Route;
use App\Http\Middleware\RedirectIfAuthenticatedMiddleware as RedirectIfAuth;
use App\Http\Middleware\RedirectIfGuestMiddleware as RedirectIfGuest;


Route::get('/', 'WelcomeController@index')->add(RedirectIfGuest::class);
Route::get('/dashboard', 'DashboardController@index')->add(RedirectIfGuest::class);
Route::get('/dashboard/own', 'DashboardController@own')->add(RedirectIfGuest::class);
Route::get('/dashboard/subscribes', 'DashboardController@subscribes')->add(RedirectIfGuest::class);


Route::get('/register', 'RegisterController@show')->add(RedirectIfAuth::class);
Route::post('/register', 'RegisterController@store')->add(RedirectIfAuth::class);

Route::get('/login', 'LoginController@show')->add(RedirectIfAuth::class);
Route::post('/login', 'LoginController@store')->add(RedirectIfAuth::class);;
Route::get('/logout', 'LoginController@logout')->add(RedirectIfGuest::class);

Route::get('/service/create', 'ServiceController@create')->add(RedirectIfGuest::class);
Route::post('/service/create', 'ServiceController@store')->add(RedirectIfGuest::class);
Route::get('/service/{id}', 'ServiceController@show')->add(RedirectIfGuest::class);

Route::get('/service/{id}/edit', 'ServiceController@edit')->add(RedirectIfGuest::class);
Route::post('/service/{id}/edit', 'ServiceController@update')->add(RedirectIfGuest::class);

Route::post('/service/remove', 'ServiceController@remove')->add(RedirectIfGuest::class);

Route::post('/service/{id}/subscribe', 'SubscribeController@add')->add(RedirectIfGuest::class);
Route::post('/service/{id}/unsubscribe', 'SubscribeController@remove')->add(RedirectIfGuest::class);
