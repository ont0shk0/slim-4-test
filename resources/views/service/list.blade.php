<table class="table table-bordered">
    <thead>
    <tr>
        <th></th>
        <th>Название</th>
        <th>Описание</th>
        <th>Автор</th>
        <th>Категория</th>
        <th>Цена</th>
        <th>Время создания</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @forelse($services as $service )
        <tr>
            <td>
                <img class="img-fluid" src="{{ $service->image_path() }}" alt="" width="50px" height="50px">
            </td>
            <td>{{ $service->title }}</td>
            <td> {{ $service->description }}</td>
            <td> {{ $service->user->name }}</td>
            <td> {{ $service->category_id }}</td>
            <td> {{ $service->price }}</td>
            <td> {{ $service->created_at }}</td>
            <td>
                <a href="/service/{{ $service->id }}">Посмотреть</a>
            </td>
        </tr>
    @empty
        <tr>
            <td colspan="8">None</td>
        </tr>
    @endforelse
    </tbody>
</table>