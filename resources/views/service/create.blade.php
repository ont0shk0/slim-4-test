@extends('layouts.app')
@section('title', 'Создать услугу')
@section('content')

        <div class="col-md-8">
                        <form action="/service/create" method="post">
                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">Название</label>
                                <div class="col-md-6">
                                    <input type="text" id="title" class="form-control" name="title" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Описание</label>
                                <div class="col-md-6">
                                    <textarea id="title" class="form-control" name="description" required></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="price" class="col-md-4 col-form-label text-md-right">Цена</label>
                                <div class="col-md-6">
                                    <input type="number" id="price" class="form-control" name="price" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category" class="col-md-4 col-form-label text-md-right">Категория</label>
                                <div class="col-md-6">
                                    <input type="number" id="category" class="form-control" name="category" required>
                                </div>
                            </div>

                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                   Создать
                                </button>
                            </div>
                        </form>
    </div>
@endsection