@extends('layouts.app')
@section('title', "Услуга - {$service->title}")
@section('content')
    <div class="row">
        @if($service->canEdit())
            <p><a href="/service/{{ $service->id }}/edit" class="btn btn-success">Изменить</a></p>
        @endif

        @if($service->canRemove())
            <form method="post" action="/service/remove">
                <input type="hidden" value="{{$service->id}}" name="id">
                <input class="btn btn-danger" type="submit" value="Удалить">
            </form>

        @endif
    </div>
    <div class="row">

        <table class="table table-bordered table-striped">
            <tbody>
            <tr>
                <th></th>
                <td>
                    <img class="img-fluid" src="{{ $service->image_path() }}" width="100px" height="50px">
                </td>
            </tr>
            <tr>
                <th>Название</th>

                <td>{{ $service->title }}</td>
            </tr>
            <tr>
                <th>Описание</th>

                <td> {{ $service->description }}</td>
            </tr>
            <tr>
                <th>Автор</th>

                <td> {{ $service->user->name }}</td>
            </tr>
            <tr>
                <th>Категория</th>

                <td> {{ $service->category_id }}</td>
            </tr>
            <tr>
                <th>Цена</th>

                <td> {{ $service->price }}</td>
            </tr>
            <tr>
                <th>Время создания</th>

                <td> {{ $service->created_at }}</td>
            </tr>

            </tbody>
        </table>
    </div>
    @if(!$service->isOwner())
        @if(!$contains)
            <div class="row">
                <form action="/service/{{ $service->id }}/subscribe" method="post">
                    <input type="submit" class="btn btn-info" value="Подписаться">
                </form>
            </div>
        @else
            <div class="row">
                <form action="/service/{{ $service->id }}/unsubscribe" method="post">
                    <input type="submit" class="btn btn-danger" value="Отписаться">
                </form>
            </div>
        @endif
    @endif


@endsection