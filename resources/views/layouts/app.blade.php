<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/simple-sidebar.css" rel="stylesheet">
    @yield('css')

</head>

<body>

<div class="d-flex" id="wrapper">

    @if (Auth::check())
    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading">Супер сайт</div>
        <div class="list-group list-group-flush">
            <a href="/dashboard" class="list-group-item list-group-item-action bg-light">Каталог</a>
            <a href="/dashboard/own" class="list-group-item list-group-item-action bg-light">Мои услуги</a>
            <a href="/dashboard/subscribes" class="list-group-item list-group-item-action bg-light">Подписки</a>

        </div>
    </div>
    <!-- /#sidebar-wrapper -->
    @endif
    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    @if (!Auth::check())
                        <li class="nav-item active">
                            <a class="nav-link" href="/register">Register <span class="sr-only">(current)</span></a>

                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="/login">Login <span class="sr-only">(current)</span></a>

                        </li>
                    @else
                        <li class="nav-item">
                            <span>{{ Auth::user()->name }}</span>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Logout <span class="sr-only">(current)</span></a>
                        </li>
                    @endif
                </ul>
            </div>
        </nav>

        <div class="container-fluid">
            <div>
               <h1> @yield('title')</h1>
            </div>
            @yield('content')
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
@yield('js')
</body>

</html>
