@extends('layouts.app')
@section('title', 'Каталог')
@section('content')
    @include('service.list', ['services' => $services])
@endsection