@extends('layouts.app')
@section('title', 'Мои подписки')
@section('content')
    @include('service.list', ['services' => $services])
@endsection