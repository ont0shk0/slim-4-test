@extends('layouts.app')
@section('title', 'Мои услуги')
@section('content')
    <p><a href="/service/create" class="btn btn-success">Добавить</a></p>
    @include('service.list', ['services' => $services])
@endsection