<?php


use Phinx\Seed\AbstractSeed;

class UserTableSeeder extends AbstractSeed
{

    public function run()
    {
        factory(App\Models\User::class, 5)->create();

    }
}
