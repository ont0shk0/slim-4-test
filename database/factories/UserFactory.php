<?php

Factory::define(\App\Models\User::class, fn ($faker) => [
    'name' => $faker->name,
    'email' => $faker->unique()->email,
    'password' => sha1('secret'),
    'role' => $faker->numberBetween(0,2),
    'created_at' => date('Y-m-d H:i:s'),
    'updated_at' => date('Y-m-d H:i:s'),
]);

