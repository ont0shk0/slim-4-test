<?php

use Phinx\Migration\AbstractMigration;

class ServiseTableAdd extends AbstractMigration
{

    public function change()
    {
        $table = $this->table('services');
        $table->addColumn('title', 'string', ['limit' => 128])
            ->addColumn('description', 'text')
            ->addColumn('category_id', 'integer',['default' => 0])
            ->addColumn('user_id', 'integer')
            ->addColumn('price', 'integer', ['default' => 0])
            ->addTimestamps()
            ->create();

    }
}
