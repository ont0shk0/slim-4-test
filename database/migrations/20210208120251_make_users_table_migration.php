<?php

use Phinx\Migration\AbstractMigration;

class MakeUsersTableMigration extends AbstractMigration {

    public function change() {

        $table = $this->table('users');
        $table->addColumn('name', 'string', ['limit' => 128])
            ->addColumn('email', 'string', ['limit' => 128])
            ->addColumn('password', 'string', ['limit' => 128])
            ->addColumn('role', 'integer', ['default' => 0])
            ->addTimestamps()
            ->create();
    }
}
