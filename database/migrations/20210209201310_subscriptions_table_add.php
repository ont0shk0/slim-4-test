<?php

use Phinx\Migration\AbstractMigration;

class SubscriptionsTableAdd extends AbstractMigration
{

    public function change()
    {
        $table = $this->table('user_service', ['id' => false]);
        $table->addColumn('user_id', 'integer')
            ->addColumn('service_id', 'integer')
            ->create();
    }
}
